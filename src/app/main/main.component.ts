import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../service/api.service';

// Model
import { Meaning } from '../model/meaning';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  ack  = 'Acknowledge';
  lineSplitData;
  contentIdForm: FormGroup;
  contentID: any = {
    contentId: ''
  };
  constructor(private fb: FormBuilder, private apiServ: ApiService) {
    this.createForm();
   }

  ngOnInit() {
  }

  createForm() {
    this.contentIdForm = this.fb.group({
      contentId: ['', Validators.required]
    });
  }

  submit(e) {
    this.contentID.contentId = this.contentIdForm.value.contentId;
    this.apiServ.findContent(this.contentID);
  }



  // submitSpace(e) {
  //   this.apiServ.getSpaces();
  // }
}


