import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { StateService } from '../service/state.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(
    private stateServ: StateService,
    private router: Router
  ) { }

  ngOnInit() {
    if (this.stateServ.isLoggedIn === false) {
      this.router.navigate(['']);
    }
  }

  submit(e) {
    
  }
}
