import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ApiService {

  dataFromServer: any;
  _baseUrl = 'http://localhost:7000';
  constructor(private _http: HttpClient) { }

  findContent(contentId: any) {
    return this._http.post(this._baseUrl + '/find', contentId)
    .subscribe(
      (data: any) => {
        this.dataFromServer = data;
        // console.log(this.dataFromServer);
      }
    );
  }

  getSpaces() {
    return this._http.get(this._baseUrl + '/space')
    .subscribe(
      (data) => {
        // console.log(data);
      }
    );
  }
}
