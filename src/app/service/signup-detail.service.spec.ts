import { TestBed, inject } from '@angular/core/testing';

import { SignupDetailService } from './signup-detail.service';

describe('SignupDetailService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SignupDetailService]
    });
  });

  it('should be created', inject([SignupDetailService], (service: SignupDetailService) => {
    expect(service).toBeTruthy();
  }));
});
