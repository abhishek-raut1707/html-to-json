import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalUser } from '../model/global-user';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

@Injectable()
export class SignupDetailService {

  baseSignupURL = 'http://localhost:7000';
  fromSignupComp;
  constructor(
    private _http: HttpClient,
    private router: Router,
    private toastrServ: ToastrService
  ) { }

  showError() {
    this.toastrServ.error(' User Credentials invalid');
  }

  signupUser(userDetailSignupObj: GlobalUser) {
    return this._http.post(this.baseSignupURL + '/signupAng', userDetailSignupObj)
    .subscribe(
      (data: any) => {
        if (data) {
          console.log(data);
          this.router.navigate(['login']);
        }
      },
      (error: Error) => {
        if (error) {
          console.log(error);
          this.router.navigate(['signup']);
        }
      }
    );
  }

  loginUser(userDetailLoginObj: GlobalUser) {
    return this._http.post(this.baseSignupURL + '/loginAng', userDetailLoginObj)
    .subscribe(
      (data: any) => {
        if (data) {
          console.log(data);
          this.router.navigate(['main']);
        }
      },
      (error: Error) => {
        if (error) {
          console.log(error);
          this.showError();
          this.router.navigate(['home']);
        }
      }
    );
  }
}
