import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class StateService {

  ipAddress: string;
  isLoggedIn = false;
  constructor(private _http: HttpClient) { }

  getIp() {
    return this._http.get('http://ipinfo.io')
    .subscribe(
      (data: any) => {
        this.ipAddress = data.ip;
      }
    );
  }
}
