import { Categories } from './categories';

export class Meaning {
    meaningID: string;
    status: string;
    meaning: string[];
    notes: string[];
    popularity: string;
    bestWTM: string;
    examples: string;
    further_suggestions: string;
}
