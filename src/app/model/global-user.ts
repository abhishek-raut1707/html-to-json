import { LoginTime } from './login-time';

export class GlobalUser {
    email: string;
    name: string;
    password?: string;
    userName: string;
    phoneNo: number;
    countryCode: string;
    city: string;
    plan: string;
    plan_history?: PlanHistory[] = [];
    provider: string;
    party?: string;
    img?: string;
    active?: boolean;
    token?: string;
    login_time: LoginTime[] = [];
}

export class PlanHistory {
    date: Date;
    plan: string;
}
