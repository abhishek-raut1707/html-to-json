import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// Auth
import { SocialLoginModule, AuthServiceConfig } from 'angular5-social-auth';
import { GoogleLoginProvider, FacebookLoginProvider, LinkedinLoginProvider } from 'angular5-social-auth';

// Components
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { MainComponent } from './main/main.component';

// Routing
import { AppRoutingModule } from './app-routing.module';

// Services
import { StateService } from './service/state.service';
import { SignupDetailService } from './service/signup-detail.service';
import { ApiService } from './service/api.service';

import { ToastrModule } from 'ngx-toastr';
import { ToastrService } from 'ngx-toastr';

export function providersConfig() {
  const configs = new AuthServiceConfig([
    {
      id: GoogleLoginProvider.PROVIDER_ID,
      provider: new GoogleLoginProvider('833806575434-ikf7p8707tj98sgk1hq5gukm80tjjeos.apps.googleusercontent.com')
    },
    {
      id: FacebookLoginProvider.PROVIDER_ID,
      provider: new FacebookLoginProvider('797304483808655')
    },
    {
      id: LinkedinLoginProvider.PROVIDER_ID,
      provider: new LinkedinLoginProvider('81018y59v8ated')
    }
  ]);

  return configs;
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SignupComponent,
    LoginComponent,
    ProfileComponent,
    MainComponent
  ],
  imports: [
    BrowserModule,
    SocialLoginModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [StateService, SignupDetailService, ToastrService, ApiService, {
    provide: AuthServiceConfig,
    useFactory: providersConfig
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
