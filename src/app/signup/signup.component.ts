import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

// Router
import { Router } from '@angular/router';

// Model
import { GlobalUser } from '../model/global-user';
import { LoginTime } from '../model/login-time';

// Services
import { SignupDetailService } from '../service/signup-detail.service';
import { StateService } from '../service/state.service';
import { GoogleLoginProvider, FacebookLoginProvider, LinkedinLoginProvider, SocialUser } from 'angular5-social-auth';
import { AuthService } from 'angular5-social-auth';

// Toast
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signupFormDetails: FormGroup;
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private stateServ: StateService,
    private signupDetailServ: SignupDetailService,
    private authServ: AuthService,
    private toastrServ: ToastrService) {
    this.createForm();
   }


  ngOnInit() {
    this.stateServ.getIp();
  }

  showError() {
    this.toastrServ.error(' Password match incorrect !!');
  }

  createForm() {
    this.signupFormDetails = this.fb.group({
      name: ['', Validators.required],
      email: ['' , Validators.email],
      password: ['', Validators.minLength(1)],
      cpass: ''
    });

  }

  submit(e) {
    const userSignupDetail = new GlobalUser;
    const login_time = new LoginTime;
    if (this.signupFormDetails.value.password !== this.signupFormDetails.value.cpass) {
      this.showError();
    } else {
      userSignupDetail.name = this.signupFormDetails.value.name;
      userSignupDetail.email = this.signupFormDetails.value.email;
      userSignupDetail.password = this.signupFormDetails.value.password;

      login_time.login_type = 'Local';
      login_time.login_date = new Date;
      login_time.ip_address = this.stateServ.ipAddress;

      userSignupDetail.login_time.push(login_time);
      this.signupDetailServ.fromSignupComp = userSignupDetail;
      console.log(' userSignupDetail local ', userSignupDetail);
      console.log(' this.signupDetailServ.fromSignupComp local ', this.signupDetailServ.fromSignupComp);
      this.stateServ.isLoggedIn = true;
      this.router.navigate(['login']);
    }
  }

  socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;
    if (socialPlatform === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    } else if (socialPlatform === 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform === 'linkedin') {
      socialPlatformProvider = LinkedinLoginProvider.PROVIDER_ID;
    }

    this.authServ.signIn(socialPlatformProvider).then(
      (user: SocialUser) => {
        const userSignupDetail = new GlobalUser;
        const login_time = new LoginTime;
        userSignupDetail.name = user.name;
        userSignupDetail.email = user.email;
        userSignupDetail.img = user.image;
        userSignupDetail.provider = user.provider;
        userSignupDetail.token = user.token;

        login_time.ip_address = this.stateServ.ipAddress;
        login_time.login_date = new Date;
        login_time.login_type = user.provider;

        userSignupDetail.login_time.push(login_time);

        this.signupDetailServ.fromSignupComp = userSignupDetail;
        console.log(' userSignupDetail social ', userSignupDetail);
        console.log(' this.signupDetailServ.fromSignupComp social ', this.signupDetailServ.fromSignupComp);
        this.stateServ.isLoggedIn = true;
        this.router.navigate(['profile']);
      }
    );
  }
}
