import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { SignupDetailService } from '../service/signup-detail.service';
import { GlobalUser } from '../model/global-user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginFormDetails: FormGroup;
  constructor(
    private fb: FormBuilder,
    private signupDetailServ: SignupDetailService) {
    this.createForm();
   }

  ngOnInit() {
  }

  createForm() {
    this.loginFormDetails = this.fb.group({
      loginEmail: ['', Validators.email],
      loginPassword: ['', Validators.minLength(1)]
    });
  }

  submit(e) {
    const userDetailLogin = new GlobalUser;
    userDetailLogin.email = this.loginFormDetails.value.loginEmail;
    userDetailLogin.password = this.loginFormDetails.value.loginPassword;

    this.signupDetailServ.loginUser(userDetailLogin);
  }

}
